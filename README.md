Pikaday Calendar
****************

-- SUMMARY --

  Pikaday calendar uses the Pikaday (https://github.com/Pikaday/Pikaday) JS
  plugin to provide a Lightweight date picker.


-- INSTALLATION --

  1. Download the Pikaday JS plugin
     (https://github.com/Pikaday/Pikaday version 1.8.2 or higher is recommended)
     and extract the file under "libraries".
  2. Download and enable the module.
  3. Configure at Administer > Configuration > User interface > Pikaday Settings
     (requires administer site configuration permission)

-- INSTALLATION VIA COMPOSER --
  It is assumed you are installing Drupal through Composer using the Drupal
  Composer facade. See https://www.drupal.org/docs/develop/using-composer/using-composer-to-manage-drupal-site-dependencies#drupal-packagist

  Before you add the module using composer, you should add an installer path
  so that the Pikaday JavaScript library is installed in the correct location.
  You might have an entry similar to below in your composer.json already if
  you had used [drupal/recommended-project](https://github.com/drupal/recommended-project)
  to create your project.
```
    "extra": {
        "installer-paths": {
            "web/libraries/{$name}": ["type:drupal-library"]
        }
    }
```
  where `web/libraries/` is the path to the libraries directory relative to your
  _project_ root. Modify the entry above to add `Pikaday/Pikaday` in that array.
```
    "extra": {
        "installer-paths": {
            "web/libraries/{$name}": [
              "type:drupal-library",
              "Pikaday/Pikaday"
            ]
        }
    }
```

  Next, you need to add a custom installer-type so that composer installer
  extended plugin can pick it up. Find the `installer-types` entry in extra
  section and add `library` to it. Your entry should look something like the
  following:
```
    "extra": {
        "installer-types": [
            "library"
        ]
    }
```

  Remember, you may have other entries in there already. For this to work, you
  need to have the 'oomphinc/composer-installers-extender' installer. If you
  don't have it, or are not sure, simply run:
```
composer require oomphinc/composer-installers-extender
```

  Then, run the following composer command:

```
composer require drupal/pikaday
```

  This command will add the Pikaday Drupal module and JavaScript library to your
  project. The library will be downloaded to the `drupal-library` installer path
  you set in the first step.


-- INTEGRATING PIKADAY --
  After configuring Pikaday from the settings, you have to ensure that element
  with id #datepicker is present in your form as currently there isn't any
  provision to select the target field as well.
