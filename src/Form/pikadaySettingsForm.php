<?php

namespace Drupal\pikaday\Form;

use Drupal\Core\Form\ConfigFormBase;
use Drupal\Core\Form\FormStateInterface;

/**
 * Configure example settings for this site.
 */
class PikadaySettingsForm extends ConfigFormBase {

  /**
   * {@inheritdoc}
   */
  public function getFormId() {
    return 'pikaday_admin_settings';
  }

  /**
   * {@inheritdoc}
   */
  protected function getEditableConfigNames() {
    return [
      'pikaday.settings',
    ];
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state) {
    $config = $this->config('pikaday.settings');

    $form['pikaday_enable'] = [
      '#type' => 'select',
      '#title' => $this->t('Enable Pikaday Calendar'),
      '#default_value' => $config->get('pikaday_enable'),
      '#options' => [
        '0' => $this
          ->t('Disable'),
        '1' => $this
          ->t('Enable'),
      ],
    ];

    $form['pikaday_cdn'] = [
      '#type' => 'checkbox',
      '#title' => $this->t('Use Pikaday CDN'),
      '#default_value' => $config->get('pikaday_cdn'),
    ];

    $form['pikaday_formids'] = [
      '#type' => 'textarea',
      '#title' => $this->t('Pikaday Calendar for Specific Forms'),
      '#default_value' => $config->get('pikaday_formids'),
      '#description' => $this->t('Add each form id per line. For example- If Form Id is `system_site_information_settings` then add it and press enter to add the next form id.'),
    ];

    return parent::buildForm($form, $form_state);
  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {
    // Retrieve and save the configuration.
    $this->configFactory->getEditable('pikaday.settings')
      ->set('pikaday_enable', $form_state->getValue('pikaday_enable'))
      ->set('pikaday_cdn', $form_state->getValue('pikaday_cdn'))
      ->set('pikaday_formids', $form_state->getValue('pikaday_formids'))
      ->save();

    parent::submitForm($form, $form_state);
  }

}
