/**
 * Pikaday Javascript.
 */
(function ($, Drupal, drupalSettings) {
  Drupal.behaviors.pikaday = {
    attach: function attach(context) {
      var $context = $(context);
      var picker = new Pikaday({ field: $('#datepicker')[0] });
    }
  };
})(jQuery, Drupal, drupalSettings);
